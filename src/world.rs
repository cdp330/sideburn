use fastrand;
use crate::sideburn::grid_nd::Lattice;
use anyhow::Result;
pub struct World {
    pub board: Lattice<2, Color>,
    rules: Vec<Rule>
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Color {
    Black = 0x000000,
    Red = 0xFF004D, 
    Blue = 0xAB5236,
    Green = 0x008751,
    White = 0xFFF1E8,
    Orange = 0xFFA300,
    Grey = 0x83769C,
}

const R: Color = Color::Red;
const B: Color = Color::Blue;
const G: Color = Color::Green;
const K: Color = Color::Black;
const W: Color = Color::White;
const O: Color = Color::Orange;

impl Default for Color {
    fn default() -> Self {
        Self::Black
    }
}


#[derive(Debug, Clone)]
pub struct Rule { 
    pattern: Lattice<2, Color>,
    replacement: Lattice<2, Color>,
}

pub fn lattice3x1(items: [Color; 3]) -> Lattice<2, Color> {
    Lattice::from(items.into(), [3,1]).unwrap()
}

impl World {
    pub fn new() -> Result<Self> {
        let mut board = Lattice::new_default([256, 256]);
        board.use_basis = true;
        board.set(R, [10, 10].into());
        
        let rules = vec![
            Rule {pattern: lattice3x1([R,K,K]), replacement: lattice3x1([W,W,R])},
            Rule {pattern: lattice3x1([R,K,W]), replacement: lattice3x1([G,W,O])},
            Rule {pattern: lattice3x1([O,W,G]), replacement: lattice3x1([O,K,B])},
            Rule {pattern: lattice3x1([B,W,W]), replacement: lattice3x1([K,K,B])},
            Rule {pattern: lattice3x1([B,W,O]), replacement: lattice3x1([K,K,R])}
        ];


        for x in 0..board.width() {
            board.set(Color::White, [x, board.height() - 1].into());
        }
        for y in 0..board.height() {
            board.set(Color::White, [board.width() - 1, y].into());
        }
        Ok(World { board, rules })
    }

    

    pub fn update2(&mut self) -> Result<()> {
        Ok(())
    }

    pub fn update(&mut self, rng: &fastrand::Rng) -> Result<()> {
        for rule in self.rules.iter() {

            let mut pattern = rule.pattern.clone();
            let all_matches = pattern.basis.all_rotations().iter()
                .flat_map(|rot| {
                    pattern.apply_basis(&rot);
                    self.board.pattern_occurrences(&pattern)
                        .map(|pt| (pt.as_i32(), rot.clone())).collect::<Vec<_>>()
                }).collect::<Vec<_>>();
            
            if all_matches.len() > 0 {
                let (pos, basis) = &all_matches[rng.usize(0..all_matches.len())];
                let mut replacement =  rule.replacement.clone();
                replacement.apply_basis(basis);
                self.board.paste(&replacement, *pos);
                break;
            }
        }


        Ok(())
    }
    
}