use super::point_nd::Point;


// Axis-aligned bounding box thingy
#[derive(Clone, Debug, Copy)]
pub struct Extent<const N: usize> {
    pub origin: Point<N, i32>,
    pub shape:  Shape<N> 
}
impl<const N: usize> Extent<N> {
    pub fn every_point(&self) -> impl Iterator<Item=Point<N, i32>> + '_ {
        self.shape.every_point().map(|pt| pt + self.origin )    
    }


    pub fn out_of_bounds(&self, pos: Point<N, i32>) -> bool {
        let pos = pos - self.origin;
        self.shape.out_of_bounds(pos)
    }

    pub fn shape_pt(&self) -> Point<N, usize> {
        self.shape.shape 
    }
}
impl<const N: usize> From<[usize; N]> for Extent<N> {
    fn from(shape: [usize; N]) -> Self {
        Self { origin: Point::zero(), shape: Shape { shape: shape.into() } }
    }
}

// Size of a N-D cuboid's walls
#[derive(Clone, Debug, Copy)]
pub struct Shape<const N: usize> {
    pub shape:  Point<N, usize> 
}



impl<const N: usize> Shape<N> {
    pub fn every_point(&self) -> impl Iterator<Item=Point<N, i32>> + '_ {
        AllPoints {
            curr_point: [0;N],
            cuboid_shape: self.shape.0,
            overflow: false
        }
        .map(|pt| pt.into())
    }

    pub fn out_of_bounds(&self, pos: Point<N, i32>) -> bool {
        self.shape.0.iter().zip(pos.0).any(|(&dim, coord)| coord >= dim as i32 || coord < 0)
    }

    // Get index into a 1D array that this shape describes
    #[inline]
    pub fn get_index(&self, pt: Point<N, i32>) -> Result<usize, ()> {
        if self.out_of_bounds(pt) {
            return Err(())
        }
        let pt = pt.as_usize();
        let index = pt.0.iter().enumerate().fold(0, |accumulated, (n, &pt_n)| {
            accumulated + (pt_n * self.shape.0.iter().take(n).product::<usize>()) 
        });
        Ok(index)
    }
}


// Yield every point within a N-dimensional cuboid with dimensions <shape>. 
// This works similar to counting in an arbitrary base.
struct AllPoints<const N: usize> {
    curr_point: [usize; N],
    cuboid_shape: [usize; N],
    overflow: bool
}

impl<const N: usize> Iterator for AllPoints<N> {
    type Item = [usize; N];

    fn next(&mut self) -> Option<Self::Item> {
        if self.overflow { return None; }
        let mut next_point = self.curr_point.clone();
        
        for (pt_i, &shape_i) in next_point.iter_mut().rev().zip(self.cuboid_shape.iter().rev()) {
            *pt_i = *pt_i + 1; 
            if *pt_i >= shape_i {
                *pt_i = 0;
            } 
            else {  
                break;
            }
        }
        if next_point == [0; N] {
            self.overflow = true;
        }
        let ret = Some(self.curr_point);
        self.curr_point = next_point;
        ret
    }
}




#[test]
fn test_1() {

    let points = AllPoints {
        curr_point: [0, 0, 0, 0],
        cuboid_shape: [3, 4, 3, 3],
        overflow: false
    }
    .collect::<Vec<_>>();
    for i in points {
        println!("{:?}", i);
    }

    let ext = Extent::from([3,3,3]);
    assert_eq!(true, ext.out_of_bounds([-1,0,0].into()));
    assert_eq!(false, ext.out_of_bounds([0,0,0].into()));
    assert_eq!(false, ext.out_of_bounds([2,2,2].into()));
    assert_eq!(true, ext.out_of_bounds([3,2,2].into()));
    assert_eq!(true, ext.out_of_bounds([-5,-5,1].into()));

}