
use std::{ops::{Add, Mul, Sub}, fmt::Display, iter::repeat};
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Point<const N: usize, T>(pub [T; N]);



impl<const N: usize, T> Add for Point<N, T> where T: Add<Output=T> + Copy {
    type Output = Self;
    fn add(mut self, rhs: Self) -> Self::Output {
        for (i, rhs_i) in self.0.iter_mut().zip(rhs.0) {
            *i = *i + rhs_i;
        }
        self
    } 
}

impl<const N: usize, T> Sub for Point<N, T> where T: Sub<Output=T> + Copy {
    type Output = Self;
    fn sub(mut self, rhs: Self) -> Self::Output {
        for (i, rhs_i) in self.0.iter_mut().zip(rhs.0) {
            *i = *i - rhs_i;
        }
        self
    } 
}

impl<const N: usize, T> Point<N, T> where T: Default + Copy {
    pub fn zero() -> Self {
        Self([T::default(); N])
    }
}

impl<const N: usize, T> Mul<T> for Point<N, T> where T: Mul<Output = T> + Copy {
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        self.0.map(|i| i * rhs).into()
    }
}

impl<const N: usize, T> From<[T; N]> for Point<N, T> {
    fn from(arr: [T; N]) -> Self {
        Self(arr)
    }
}

impl<const N: usize> From<[usize; N]> for Point<N, i32> {
    fn from(arr: [usize; N]) -> Self {
        Point(arr).into()
    }
}

impl<const N: usize, T> From<Point<N, T>> for [T; N] {
    fn from(pt: Point<N, T>) -> Self {
        pt.0
    }
}

impl<const N: usize> From<Point<N, usize>> for Point<N, i32> {
    fn from(pt: Point<N, usize>) -> Self {
        pt.0.map(|i| i as i32).into()
    }
}

impl<const N: usize> From<Point<N, i32>> for Point<N, usize> {
    fn from(pt: Point<N, i32>) -> Self {
        pt.0.map(|i| i as usize).into()
    }
}

impl<const N: usize> Point<N, usize> {
    pub fn as_i32(self) -> Point<N, i32> {
        self.into()
    }
}

impl<const N: usize> Point<N, i32> {
    pub fn as_usize(self) -> Point<N, usize> {
        self.into()
    }

    pub fn as_usize_if_positive(self) -> Option<Point<N, usize>> {
        if self.0.iter().any(|&i| i < 0) {
            return None;
        }
        Some(self.into())
    }
}

#[cfg(test)]
mod point_tests {
    use super::*;

    #[test]
    fn test_1() {
        let a: Point<3, _> = [3, 4, 5].into();
        
        assert_eq!(Point([3,3,3]), Point([2,0,1]) + Point([1,3,2]));
        let b: Point<3, i32> = Point::zero();
        assert_eq!(Point([0,0,0]), b);
    }


}