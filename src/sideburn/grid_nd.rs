
use super::extent::{Shape, Extent};

use super::point_nd::Point;
use super::basis::LatticeBasis;



#[derive(Clone)]
pub struct Lattice<const N: usize, T> {
    arr: Box<[T]>,

// Both extents should always have origin of zero. Thus they are not public.
    extent: Extent<N>,                
    original_extent: Extent<N>,        // Extent is allowed to change shape during basis change. This one stays constant.

    pub basis: LatticeBasis<N>, 
    pub use_basis: bool
}

impl<T, const N: usize> Lattice<N, T> {
    #[inline]
    pub fn dims(&self) -> [usize; N] {
        self.extent.shape.shape.0
    }    

    
    pub fn from(data: Vec<T>, dimensions: [usize; N]) -> Result<Self, String> {
        if data.len() != dimensions.iter().product() {
            return Err("Data length does not match dimension product!".into())
        } 
        Ok(Self {
            arr: data.into(),
            extent: dimensions.into(),
            original_extent: dimensions.into(),
            basis: LatticeBasis::default(), 
            use_basis: true
        })
    }

    pub fn get(&self, pt: Point<N, i32>) -> Option<&T> {
        let mut pt = pt.as_usize_if_positive()?;

        if self.use_basis {
            pt = self.basis.remap_pt_wrap(pt.0, self.original_extent.shape_pt().0).into()
        }
        if let Ok(index) = self.original_extent.shape.get_index(pt.as_i32()) {
            return Some(&self.arr[index])
        }
        None
    }

    pub fn set(&mut self, item: T, pt: Point<N, i32>) -> Result<(), ()> {
        if let Ok(index) = self.original_extent.shape.get_index(pt) {
            self.arr[index] = item;
            return Ok(())
        }
        Err(())
    }


}


impl<T, const N: usize> Lattice<N, T> {

    // Obtain a window into this lattice and give everything within it. 
    pub fn every_pt_item_window<'a: 'b, 'b>(&'a self, extent: &'b Extent<N>) -> impl Iterator<Item=(Point<N,i32>, &T)> + 'b  {
        extent.every_point()
            .map(|pt| (pt, self.get(pt)))
            .filter(|(_, item)| item.is_some())
            .map(|(pt, item)| (pt, item.unwrap()))
    }

    pub fn every_item_window<'a: 'b, 'b>(&'a self, extent: &'b Extent<N>) -> impl Iterator<Item=&T> + 'b {
        self.every_pt_item_window(extent).into_iter().map(|(_, item)| item)
    }

    pub fn every_pt_item(&self) -> impl Iterator<Item=(Point<N,i32>, &T)> + '_ {
        self.every_pt_item_window(&self.extent)
    }

    pub fn every_item(&self) -> impl Iterator<Item=&T> + '_ {
        self.every_item_window(&self.extent)
    }


    pub fn apply_basis(&mut self, basis: &LatticeBasis<N>) {
        self.basis = basis.clone();
        self.extent.shape.shape.0 = self.basis.transform_shape(self.original_extent.shape_pt().0);
    }

}

impl<T, const N: usize> Lattice<N, T> where T: Copy {

    pub fn paste(&mut self, patt: &Self, position: Point<N, i32>) {
        for (pt, color) in patt.every_pt_item() {
            let target_pt = position + pt;
            self.set(*color, target_pt);
        }
    }
}
impl<T, const N: usize> Lattice<N, T> where T: Eq  {

    pub fn has_pattern_at_pos(&self, pattern: &Self, position: Point<N, i32>) -> bool {

        let new_extent = Extent {
            origin: position,
            shape: pattern.extent.shape,
        };

        self.every_item_window(&new_extent).eq(pattern.every_item())
    }

    // Search for where pattern appears naively
    pub fn pattern_occurrences<'a: 'b, 'b>(&'a self, pattern: &'b Lattice<N, T>) -> impl Iterator<Item=Point<N, usize>> + 'b {
        self.extent.every_point()
            .filter(move |pt| self.has_pattern_at_pos(&pattern, *pt))
            .map(|pt| pt.as_usize())
    }
}
impl<T> Lattice<2, T> {

    pub fn width(&self) -> usize  {self.extent.shape.shape.0[0]}
    pub fn height(&self) -> usize {self.extent.shape.shape.0[1]}


    pub fn rotate_once(mut self) -> Self {

        self.basis = self.basis.rotate();
        self.extent = self.basis.transform_shape(self.original_extent.shape_pt().0).into();
        self
    }

}

impl<T, const N: usize> Lattice<N, T> where T: Default + Clone {
    pub fn new_default(dimensions: [usize; N]) -> Self {
        Self {
            arr: vec![T::default(); dimensions.iter().product()].into(),
            extent: dimensions.into(),
            original_extent: dimensions.into(), 
            basis: LatticeBasis::default(),
            use_basis: true
        }
    }
}






#[cfg(test)]
mod point_tests {
    use super::*;


    #[test]
    fn test_2() { 
        let a: Lattice<3, ()> = Lattice::new_default([3,3,4]);
        assert_eq!(a.extent.out_of_bounds([1i32, 1, 1].into()), false);
        assert_eq!(a.extent.out_of_bounds([3i32, 1, 1].into()), true);
        assert_eq!(a.extent.out_of_bounds([1i32, 3, 1].into()), true);
        assert_eq!(a.extent.out_of_bounds([1i32, 1, 4].into()), true);
        assert_eq!(a.extent.out_of_bounds([5i32, 5, 5].into()), true);

        let a: Lattice<2, ()> = Lattice::new_default([2,2]);
        assert_eq!(a.original_extent.shape.get_index([0usize,0].into()), Ok(0));
        assert_eq!(a.original_extent.shape.get_index([1usize,0].into()), Ok(1));
        assert_eq!(a.original_extent.shape.get_index([2usize,0].into()), Err(()));
        assert_eq!(a.original_extent.shape.get_index([0usize,1].into()), Ok(2));
        assert_eq!(a.original_extent.shape.get_index([1usize,1].into()), Ok(3));
        assert_eq!(a.original_extent.shape.get_index([1usize,4].into()), Err(()));



    }
    use super::super::point_nd::Point;

    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    pub enum Color {
        Black = 0x000000,
        Red = 0xFF004D, 
        Blue = 0xAB5236,
        Green = 0x008751,
        White = 0xFFF1E8,
        Orange = 0xFFA300,
        Grey = 0x83769C,
    }
    #[test]
    fn test_rotation() {
        let patt = Lattice::from(vec![Color::Green, Color::Green, Color::Red], [1,3]).unwrap();
        println!("{:?}", patt);

        assert_eq!(Point([1,3]), patt.extent.shape_pt());

        assert_eq!(Some(&Color::Green), patt.get([0i32, 0].into()));
        assert_eq!(Some(&Color::Green), patt.get([0i32, 1].into()));
        assert_eq!(Some(&Color::Red), patt.get([0i32, 2].into()));

        let patt = patt.rotate_once();

        println!("{:?}", patt);
        assert_eq!(Point([3,1]), patt.extent.shape_pt());
        assert_eq!(Some(&Color::Green), patt.get([0, 0].into()));
        assert_eq!(Some(&Color::Green), patt.get([1, 0].into()));
        assert_eq!(Some(&Color::Red), patt.get([2, 0].into()));


        let patt = patt.rotate_once();
        println!("{:?}", patt);
        assert_eq!(Some(&Color::Red), patt.get([0, 0].into()));
        assert_eq!(Some(&Color::Green), patt.get([0, 1].into()));
        assert_eq!(Some(&Color::Green), patt.get([0, 2].into()));
    }
}


use std::fmt;
use std::fmt::Debug;
use std::fmt::{Formatter, Error};
// Draw the grid out in 2D
impl<T> fmt::Debug for Lattice<2, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        write!(f, "{:?}", self.extent)?;
        write!(f, "{:?}", self.basis)?;
        for y in 0..self.height() {
            write!(f, "\r\n")?;
            for x in 0..self.width() {
                match self.get([x,y].into()) {
                    None => write!(f, "  ")?,
                    Some(_) => write!(f, "██")?,
                }
            }
        }
        write!(f, "\r\n")?;
        Ok(())
    }
}
