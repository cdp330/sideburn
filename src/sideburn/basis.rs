use core::fmt::Debug;
use core::fmt::Formatter;
use core::fmt;

use super::extent::Extent;
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct BasisElem {
    index: usize, 
    positive: bool
}
impl BasisElem {
    fn invert(self) -> Self {
        Self { index: self.index, positive: !self.positive }
    }
}

#[derive(Clone)]
pub struct LatticeBasis<const N: usize> {
    pub basis: [BasisElem; N]
}

impl<const N: usize> Debug for LatticeBasis<N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Basis: ")?;
        for i in self.basis {
            if i.positive {
                write!(f, "{}, ", i.index)?;
            }
            else {
                write!(f, "-{}, ", i.index)?;
            }
        }
        Ok(())
    }
}
impl<const N: usize> LatticeBasis<N> {
    // Transform a point using this basis. If result coords ever go negative, wrap them by a dimension.

    #[inline]
    pub fn remap_pt_wrap(&self, pt: [usize; N], dimensions: [usize; N]) -> [usize; N] {
        let mut new_pt = [0usize; N];
        
        for ((new_pt_i, basis_i), dim_i) in new_pt.iter_mut().zip(self.basis).zip(dimensions) {
            *new_pt_i = pt[basis_i.index];

            if !basis_i.positive {
                *new_pt_i = dim_i - *new_pt_i - 1;
            }
        }
        new_pt
    }

    // Transform the shape of an extent via this basis
    pub fn transform_shape(&self, shape: [usize; N]) -> [usize; N] {
        let mut new_shape = [0usize; N];
        
        for (new_pt_i, basis_i) in new_shape.iter_mut().zip(self.basis) {
            *new_pt_i = shape[basis_i.index];
        }
        new_shape
    }


}
impl<const N: usize> Default for LatticeBasis<N> {
    fn default() -> Self {
        Self {basis: (0..N).map(|i| BasisElem { index: i, positive: true }).collect::<Vec<_>>().try_into().expect("Error building basis!")} 
    }


}
impl LatticeBasis<2> {
    pub fn rotate(&self) -> Self {
        Self {basis: [self.basis[1].invert(), self.basis[0] ]}
    }
    pub fn all_rotations(&self) -> [Self; 4] {
        [self.clone(), self.rotate(), self.rotate().rotate(), self.rotate().rotate().rotate()]
    }
}


#[test]
fn test_1() {
    let a: LatticeBasis<4> = LatticeBasis::default();

    println!("{:?}", a)
}

#[test]
fn test_basis() {
    let a: LatticeBasis<2> = LatticeBasis::default().rotate();
    let dim = [2, 3];

    assert_eq!([BasisElem{ index: 1, positive: false}, BasisElem {index: 0, positive: true}], a.basis);
    assert_eq!([3, 2], a.transform_shape(dim));
    assert_eq!([2, 3], a.transform_shape([3,2]));
    assert_eq!([0, 2], a.remap_pt_wrap([2,1], dim));
    assert_eq!([1, 2], a.remap_pt_wrap([2,0], dim));
    assert_eq!([1, 0], a.remap_pt_wrap([0,0], dim));



    println!("{:?}", Extent::from([3,2]).every_point().collect::<Vec<_>>())
    // assert_eq!([)
}