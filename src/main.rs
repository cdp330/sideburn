use anyhow::{Result, Ok, Error};
use anyhow::anyhow;
mod world;
mod sideburn;
use sdl2::pixels::{PixelFormat, PixelFormatEnum};
use sdl2::{
    event::Event,
    EventPump, keyboard::Keycode,
    rect::Rect,
    pixels::Color
};

pub trait ResultStringConvert<T> {
    fn e(self) -> core::result::Result<T, Error>;
}

impl<T> ResultStringConvert<T> for core::result::Result<T, String> {
    fn e(self) -> Result<T, Error> {
        return self.map_err(|e| anyhow!(e))
    }
}
const PIXEL_SCALE: usize = 3;
fn main() -> Result<()> {

    let sdl = sdl2::init().e()?;
    let video_subsys = sdl.video().e()?;
    let window = video_subsys.window("Window", 1920, 1080)
        .position_centered()
        .build()?;

    let mut canvas = window.into_canvas()
        .target_texture()
        .present_vsync()
        .build()?;

    let rng = fastrand::Rng::new();

    let mut event_pump = sdl.event_pump().e()?;
    let format = PixelFormat::try_from(PixelFormatEnum::BGR24).e()?;
    let mut world = world::World::new()?;

    'main: loop {

        for event in event_pump.poll_iter() {
            if let Event::Quit { .. } | Event::KeyDown {keycode: Some(Keycode::Escape), ..} = event {
                break 'main;
            }
            if let Event::KeyDown {keycode: Some(code), ..} = event {
                match code {
                    Keycode::E => {
                        world = world::World::new()?;
                    }
                    _ => {}
                }
            }
        }

        for i in 0..3 {

            world.update(&rng);
        }
        canvas.set_draw_color(Color::from_u32(&format, 0x1D2B53));
        canvas.clear();

        for y in 0..world.board.height() {
            for x in 0..world.board.width() {
                let mut pixel = &world::Color::Black;
                if let Some(item) = world.board.get([x, y].into()) {
                    pixel = item;
                }
                canvas.set_draw_color(Color::from_u32(&format, *pixel as u32));
                canvas.fill_rect(Rect::new((PIXEL_SCALE * x) as i32, (PIXEL_SCALE * y) as i32, PIXEL_SCALE as u32, PIXEL_SCALE as u32)).e()?;
            }
        }
        canvas.present();
    }
    Ok(())
}


