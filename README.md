
Essentially a Rust implementation of the simplest features of [this project](https://github.com/mxgmn/MarkovJunior).

<img src="img/record.gif"></img>


In a nutshell,

$$\colorbox{red}{.}\colorbox{black}{.} \colorbox{black}{.} \rightarrow \colorbox{white}{.} \colorbox{white}{.} \colorbox{red}{.}$$
$$\colorbox{red}{.}\colorbox{black}{.} \colorbox{white}{.} \rightarrow \colorbox{green}{.} \colorbox{white}{.} \colorbox{orange}{.}$$
$$\colorbox{orange}{.}\colorbox{white}{.} \colorbox{green}{.} \rightarrow \colorbox{orange}{.} \colorbox{black}{.} \colorbox{blue}{.}$$
$$\colorbox{blue}{.}\colorbox{white}{.} \colorbox{white}{.} \rightarrow \colorbox{black}{.} \colorbox{black}{.} \colorbox{blue}{.}$$
$$\colorbox{blue}{.}\colorbox{white}{.} \colorbox{orange}{.} \rightarrow \colorbox{black}{.} \colorbox{black}{.} \colorbox{red}{.}$$


The grid starts with a randomly placed seed pixel. Every frame all areas of the grid are scanned for any matching patterns, for all rotations. Matches are replaced in order. The pattern is currently set in `world.rs`, line 50.

This project ended up with 70% of the time being sunk into making the 'perfect' N-D grid class. The grid uses const generics, supports abstract rotations (only in 2D) and implements functions / iterators for a bunch of pattern matching operations.